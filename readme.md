# Overview #

This package provides an advanced satellite navigation sensor plugin for Gazebo. The plugin provides realistic (non-Gaussian) position and velocity error perturbations, which are calculated from broadcast satellite positions and clock estimates, ionospheric delays/advances, tropospheric delays, and receiver clock bias / noise.

The plugin uses [IGS](http://www.igs.org/) MGEX and IONEX products to generate code and carrier phase measurements on up to three different frequencies to satellite vehicles from multiple constellations (GPS, GLONASS, Galileo, QZSS and Compass). It then streams these measurements in RTCM3 format over TCP to RTKLIB, where they are processed in real time to obtain a navigation solution. The navigation solution is then passed back over TCP to the sensor plugin, where it is compared to the truthful position to obtain position and velocity errors. The plugin attaches a function callback to the Gazebo GPS sensor noise model to return the calculated error in place of a normal distribution.

The key advantage with this plugin is that it produces position solution errors which are correlated in both space and time. This provides users with the ability to simulate complex GNSS configurations, such as static and kinematic RTK. It also allows one to produce synthetic GNSS measurements that can be used in models that exploit correlated noise.

There are a great number of error sources in satellite navigation. Currently, this plugin does not model path loss (signal to noise ratio), integer ambiguity, cycle slip, receiver / satelllite antenna phase center offset / variation, solid earth tides, pole tides, earth rotation, ocean loading, phase wind-up, differential code biases and multipath.

# License #

Copyright (c) Andrew Symington, 2015. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Installation instructions #

Currently, only Ubuntu 14.04 64bit is supported. However, it is expected that the code will also work with other Ubuntu versions with minor modification. The code itself depends on correctly installing four dependencies -- ```faketime```, ```ROS```, ```RTKLIB```, and ```gazebo```. Presently, RTKLIB and gazebo must have patches applied to work correctly.

Firstly, install faketime and wine:

```
$ sudo apt-get install faketime faketime:i386 wine
```

Then, [install ROS Indigo](http://wiki.ros.org/indigo/Installation/Ubuntu) using the official instructions. Make sure that you remove gazebo after installing, as we need to build and install a patched version of this simulator:

```
$ sudo apt-get remove ros-indigo-gazebo*
```

Then, checkout the crates_gnss and gazebo_ros code into a new catkin_workspace:

```
$ mkdir -p ~/catkin_ws/src
$ cd ~/catkin_ws/src
$ catkin_init_workspace
$ git clone git@bitbucket.org:asymingt/crates_gnss.git
$ git clone -b indigo-devel https://github.com/ros-simulation/gazebo_ros_pkgs.git
```

Then, checkout a patched RTKLIB

```
$ mkdir -p ~/catkin_ws/thirdparty
$ git clone -b rtklib_2.4.3 https://github.com/asymingt/RTKLIB.git 
```

Now, compile and install Gazebo from source using the [official instructions](http://gazebosim.org/tutorials?tut=install_from_source&cat=install). Make sure that you install the SDF and Gazebo default branches to ```/usr/local```.


```
$ hg clone ssh://hg@bitbucket.org/osrf/sdformat
$ push sdformat
$ mkdir build
$ cd build
$ cmake -DCMAKE_INSTALL_PREFIX=/usr/local .. 
$ make
$ sudo make install
$ pop
$ mkdir -p ~/catkin_ws/thirdparty
$ hg clone ssh://hg@bitbucket.org/osrf/gazebo
$ push gazebo
$ mkdir build
$ cd build
$ cmake -DCMAKE_INSTALL_PREFIX=/usr/local .. 
$ make
$ sudo make install
$ pop
```

Finally, build the catkin workspace:

```
$ cd ~/catkin_ws
$ catkin_make
```

# Running the example simulation #

Start by launching the example:

```
$ cd ~/catkin_ws
$ source devel/setup.bash
$ roslaunch crates_gnss example.launch
```

You should now see some debug messages in the console showing the navigation error:

```
[Wrn] [Plugin.cc:123] ERROR: Latitude   (m)   : -1.83022
[Wrn] [Plugin.cc:124] ERROR: Longitude  (m)   : 2.03383
[Wrn] [Plugin.cc:125] ERROR: Altitude   (m)   : 5.26391
[Wrn] [Plugin.cc:126] ERROR: Velocity E (m/s) : -2.71235
[Wrn] [Plugin.cc:127] ERROR: Velocity N (m/s) : 4.04443
[Wrn] [Plugin.cc:128] ERROR: Velocity U (m/s) : 0.749401
```

Alternatively, you can lprevent the automatic launch of RTKLIB with the parameter ```rtklib:=false```. Then, you can manually launch rtknavi.exe through wine and load one of the configuration files (example: ```~/catkin_ws/src/crates_gnss/share/example/conf/code_autonomous.conf```):

In Terminal 1:

```
$ cd ~/catkin_ws
$ source devel/setup.bash
$ roslaunch crates_gnss example.launch rtklib:=false
```

In Terminal 2:

```
$ cd ~/catkin_ws/thirdparty/RTKLIB/bin
$ faketime '2014-10-10 14:31:00' wine rtknavi.exe
```

Here is a screenshot of RTKLIB processing the pseudorange measurements to GPS satellites at a 15 degree elevation on 10/10/2014. 

![works.png](https://bitbucket.org/repo/xeXpzG/images/3435365043-works.png)

By default the launch script loads a code phase solution. Have a look in the```~/catkin_ws/src/crates_gnss/share/example/conf``` directory to see other possible configurations. For example, to perform L1/L2 kinematic RTK with two stations:

```
$ cd ~/catkin_ws
$ source devel/setup.bash
$ roslaunch crates_gnss example.launch config:=dual_carrier_kinematic.conf
```

# Simulating for a custom day and time #

The source code includes GPS, GLONASS, Compass, Galileo and QZSS broadcast ephemerides (brdm2830.14p), precise clocks (com18135.clk) and ephemerides (com18135.sp3), and the worldwide ionosphere total electron count (igsg2830.14i) for October 10, 2014. 

To simulate a GNSS sensor on a custom day of your choosing, you will need to first download IGS products. To do this you will need to know the year (YYYY or YY), GPS week (WWWW), day of week (D), as well as the day of year (JJJ) corresponding to the date of your choosing. You can find this easily using a [GPS calendar](http://adn.agi.com/GNSSWeb). For example, January 16, 2015 is YYYY=2015, YY=15, WWW=1827, D=5 and YYY=16. Now, download and unzip the following files into ```~/catkin_ws/src/crates_gnss/share/example/resources/gnss```

1. Broadcast ephemerides - ftp://cddis.gsfc.nasa.gov/gnss/data/campaign/mgex/daily/rinex3/YYYY/brdm/brdmJJJ0.YYp.Z
1. Precise clocks - ftp://cddis.gsfc.nasa.gov/pub/gps/products/mgex/WWWW/comWWWWD.clk.Z
1. Precise ephemerides - ftp://cddis.gsfc.nasa.gov/pub/gps/products/mgex/WWWW/comWWWWD.sp3.Z
1. Total electron count - ftp://cddis.gsfc.nasa.gov/pub/gps/products/ionex/YYYY/JJJ/igsgJJJ0.YYi

Then, modify the ```<peph>```, ```<pclk>```, ```<beph>``` and ```<ftec>```  SDF elements accordingly in ```~/catkin_ws/src/crates_gnss/share/example/models/bs/model.sdf``` and ```~/catkin_ws/src/crates_gnss/share/example/models/receiver/model.sdf```. There are many other options, which are shown below:

```
xml
<plugin name="crates_gnss_plugin" filename="libcrates_gnss_plugin.so">
  <debug>1</debug> <!-- Print some debug information (position and velocity error) -->
  <freq>10</freq>  <!-- Rate at which to stream observables -->
  <base>0</base>   <!-- Rate at which to send base station parameters (0=not a base station) -->
  <mask>1</mask>   <!-- Do not broadcast NAV or OBS for satellites below horizon -->
  <rtcmport>127.0.0.1:10001</rtcmport>  <!-- TCP host:port on which to broadcast RTCM encoded nav/obs for RTKLIB -->
  <solnport>127.0.0.1:10002</solnport>  <!-- TCP host:port on which to receive RTKLIB solutions in the ECEF frame -->
  <peph>file://gnss/com18135.sp3</peph> <!-- Precise ephemerides     -->
  <pclk>file://gnss/com18135.clk</pclk> <!-- Precise clocks          -->
  <beph>file://gnss/brdm2830.14p</beph> <!-- Broadcast ephemerides   -->
  <ftec>file://gnss/igsg2830.14i</ftec> <!-- Total electron count    -->
  <trop>file://gnss/hert2830.14zpd</trop> <!-- Trop: zenith path delay -->
  <constellations>
    <system id="GPS" code1="L1C" code2="L2C" code3="L5I"/> <!-- Global Positioning System (United States) -->
    <system id="GLO" code1="L1C" code2="L2C" code3="L5I"/> <!-- Globalnaya Navigatsionnaya Sputnikovaya Sistema (Russia) -->
    <system id="GAL" code1="L1C" code2="L5I" code3="L6C"/> <!-- Galileo (Europe) -->
    <system id="QZS" code1="L1C" code2="L2C" code3="L5I"/> <!-- Quasi-Zenith Satellite System (Japan) -->
    <system id="CMP" code1="L1I" code2="L2I" code3="L3I"/> <!-- BeiDou Navigation Satellite System (China) -->
  </constellations>
  <code>
    <noise type="gaussian">
      <mean>0.0</mean>
      <stddev>3.000</stddev>
      <bias_mean>0.0</bias_mean>
      <bias_stddev>300000</bias_stddev>
    </noise>
  </code>
  <carr>
    <noise type="gaussian">
      <mean>0.0</mean>
      <stddev>0.003</stddev>
      <bias_mean>0.0</bias_mean>
      <bias_stddev>300000</bias_stddev>
    </noise>
  </carr>
</plugin>
```

Then, modify the starting world simulation time to be a starting UTC time in the day covered by the traces you downloaded. This is very important, as the GNSS plugin needs a shared sense of global time across all sensors. You can find the UTC time using a [Unix timestamp calculator](http://www.unixtimestamp.com/). For example, 14:31:00 on October 10, 2014 equals 1412951480. This value is placed in the ```~/catkin_ws/src/crates_gnss/share/example/example.world``` file in the following way:

```
<state world_name='default'>
  <sim_time>1412951480 0</sim_time>
</state>
```

Lastly, you will need to supply a fake time to RTKLIB, as the protocol used to transmit GNSS data to and from Gazebo does not include an absolute date. To achieve this, we need to set the ```FAKE_TIME_START``` environment variable before launching rtklib through the faketime application. This is done by modifying the launch file ```~/catkin_ws/src/crates_gnss/launch/rtklib.launch```:

```
<launch>
  <arg name="conf" default="$(find crates_gnss)/share/example/conf/code_autonomous.conf"/>
  <node name="rtkrcv" pkg="crates_gnss" type="rtklib" respawn="false" output="screen" args="-s -o $(arg conf)">
        <env name="FAKE_TIME_START" value="2014-10-10 14:31:00"/>
  </node>
</launch>
```